from celebrerclient.common import utils


def do_list_services(ic, args):
    """Get services list"""
    result = ic.actions.list_services()
    columns = ['ID', 'Name', 'Component', 'Status', 'Node']
    utils.print_list(result, columns)


def do_list_reports(ic, args):
    """Get reports list"""
    result = ic.actions.list_reports()
    columns = ['ID','component_name','status', 'action', 'service_list', 'created', 'updated']
    utils.print_list(result, columns)


@utils.arg("report_id", metavar='<REPORT_ID>', help='Report ID')
def do_show_report(ic, args):
    """Get detail information about report"""
    result = ic.actions.list_services(args.report_id)
    columns = ['Name']
    utils.print_list(result, columns)


@utils.arg('services', metavar="service-1,service-2,service-3", action='append',
           default=[], help='Services list')
def do_run_services(ic, args):
    """Run service under coverage"""
    result = ic.actions.run_services(arguments=args.services[0].split(','))
    columns = ['ID', 'Services']
    utils.print_list(result, columns)


@utils.arg("task_id", metavar='<TASK_ID>', help='Task ID')
def do_stop_services(ic, args):
    """Run service under coverage"""
    result = ic.actions.stop_services(arguments=args.task_id)
#    columns = ['ID', 'Services']
#    utils.print_list(result, columns)



@utils.arg("report_id", metavar='<REPORT_ID>', help='Report ID')
def do_download_report(ic, args):
    """Get detail information about report"""
    result = ic.actions.download_reports(args.report_id)
    if result:
        file_report_name = '%s.tar.gz' % args.report_id
        print "Saving report as %s " % file_report_name
        with open(file_report_name, 'w') as file_report:
            file_report.write(report)
        print "Finished"
    


